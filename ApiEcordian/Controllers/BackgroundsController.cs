﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class BackgroundsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Backgrounds
        public IQueryable<Background> GetBackgrounds()
        {
            return db.Backgrounds;
        }

        // GET: api/Backgrounds/5
        [ResponseType(typeof(Background))]
        public async Task<IHttpActionResult> GetBackground(Guid id)
        {
            Background background = await db.Backgrounds.FindAsync(id);
            if (background == null)
            {
                return NotFound();
            }

            return Ok(background);
        }

        // PUT: api/Backgrounds/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBackground(Guid id, Background background)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != background.Id)
            {
                return BadRequest();
            }

            db.Entry(background).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BackgroundExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Backgrounds
        [ResponseType(typeof(Background))]
        public async Task<IHttpActionResult> PostBackground(Background background)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Backgrounds.Add(background);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BackgroundExists(background.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = background.Id }, background);
        }

        // DELETE: api/Backgrounds/5
        [ResponseType(typeof(Background))]
        public async Task<IHttpActionResult> DeleteBackground(Guid id)
        {
            Background background = await db.Backgrounds.FindAsync(id);
            if (background == null)
            {
                return NotFound();
            }

            db.Backgrounds.Remove(background);
            await db.SaveChangesAsync();

            return Ok(background);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BackgroundExists(Guid id)
        {
            return db.Backgrounds.Count(e => e.Id == id) > 0;
        }
    }
}