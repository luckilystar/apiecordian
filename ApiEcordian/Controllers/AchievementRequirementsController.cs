﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class AchievementRequirementsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/AchievementRequirements
        public IQueryable<AchievementRequirement> GetAchievementRequirements()
        {
            return db.AchievementRequirements;
        }

        // GET: api/AchievementRequirements/5
        [ResponseType(typeof(AchievementRequirement))]
        public async Task<IHttpActionResult> GetAchievementRequirement(Guid id)
        {
            AchievementRequirement achievementRequirement = await db.AchievementRequirements.FindAsync(id);
            if (achievementRequirement == null)
            {
                return NotFound();
            }

            return Ok(achievementRequirement);
        }

        // PUT: api/AchievementRequirements/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAchievementRequirement(Guid id, AchievementRequirement achievementRequirement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != achievementRequirement.Id)
            {
                return BadRequest();
            }

            db.Entry(achievementRequirement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AchievementRequirementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AchievementRequirements
        [ResponseType(typeof(AchievementRequirement))]
        public async Task<IHttpActionResult> PostAchievementRequirement(AchievementRequirement achievementRequirement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AchievementRequirements.Add(achievementRequirement);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AchievementRequirementExists(achievementRequirement.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = achievementRequirement.Id }, achievementRequirement);
        }

        // DELETE: api/AchievementRequirements/5
        [ResponseType(typeof(AchievementRequirement))]
        public async Task<IHttpActionResult> DeleteAchievementRequirement(Guid id)
        {
            AchievementRequirement achievementRequirement = await db.AchievementRequirements.FindAsync(id);
            if (achievementRequirement == null)
            {
                return NotFound();
            }

            db.AchievementRequirements.Remove(achievementRequirement);
            await db.SaveChangesAsync();

            return Ok(achievementRequirement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AchievementRequirementExists(Guid id)
        {
            return db.AchievementRequirements.Count(e => e.Id == id) > 0;
        }
    }
}