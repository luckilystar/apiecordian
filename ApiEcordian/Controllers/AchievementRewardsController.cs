﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class AchievementRewardsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/AchievementRewards
        public IQueryable<AchievementReward> GetAchievementRewards()
        {
            return db.AchievementRewards;
        }

        // GET: api/AchievementRewards/5
        [ResponseType(typeof(AchievementReward))]
        public async Task<IHttpActionResult> GetAchievementReward(Guid id)
        {
            AchievementReward achievementReward = await db.AchievementRewards.FindAsync(id);
            if (achievementReward == null)
            {
                return NotFound();
            }

            return Ok(achievementReward);
        }

        // PUT: api/AchievementRewards/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAchievementReward(Guid id, AchievementReward achievementReward)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != achievementReward.Id)
            {
                return BadRequest();
            }

            db.Entry(achievementReward).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AchievementRewardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AchievementRewards
        [ResponseType(typeof(AchievementReward))]
        public async Task<IHttpActionResult> PostAchievementReward(AchievementReward achievementReward)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AchievementRewards.Add(achievementReward);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AchievementRewardExists(achievementReward.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = achievementReward.Id }, achievementReward);
        }

        // DELETE: api/AchievementRewards/5
        [ResponseType(typeof(AchievementReward))]
        public async Task<IHttpActionResult> DeleteAchievementReward(Guid id)
        {
            AchievementReward achievementReward = await db.AchievementRewards.FindAsync(id);
            if (achievementReward == null)
            {
                return NotFound();
            }

            db.AchievementRewards.Remove(achievementReward);
            await db.SaveChangesAsync();

            return Ok(achievementReward);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AchievementRewardExists(Guid id)
        {
            return db.AchievementRewards.Count(e => e.Id == id) > 0;
        }
    }
}