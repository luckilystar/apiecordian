﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class TrashLogsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TrashLogs
        public IQueryable<TrashLog> GetTrashLog()
        {
            return db.TrashLog;
        }
        public IQueryable<TrashLog> GetTrashLog(DateTime minn, DateTime maxx)
        {
            //DateTime input format YYYY-DD-MM
            return (from p in db.TrashLog
                    where p.Time >= minn && p.Time <= maxx
                    select p);
        }

        public IQueryable<TrashLog> GetMyTrashLog(Guid UserID, DateTime minn, DateTime maxx)
        {
            //DateTime input format YYYY-DD-MM
            return (from p in db.TrashLog
                    where p.Time >= minn && p.Time <= maxx  &&p.Id==UserID
                    select p);
        }

        // GET: api/TrashLogs/5
        [ResponseType(typeof(TrashLog))]
        public async Task<IHttpActionResult> GetTrashLog(Guid id)
        {
            TrashLog trashLog = await db.TrashLog.FindAsync(id);
            if (trashLog == null)
            {
                return NotFound();
            }

            return Ok(trashLog);
        }

        // PUT: api/TrashLogs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTrashLog(Guid id, TrashLog trashLog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trashLog.Id)
            {
                return BadRequest();
            }

            db.Entry(trashLog).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrashLogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TrashLogs
        [ResponseType(typeof(TrashLog))]
        public async Task<IHttpActionResult> PostTrashLog(TrashLog trashLog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TrashLog.Add(trashLog);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TrashLogExists(trashLog.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = trashLog.Id }, trashLog);
        }

        // DELETE: api/TrashLogs/5
        [ResponseType(typeof(TrashLog))]
        public async Task<IHttpActionResult> DeleteTrashLog(Guid id)
        {
            TrashLog trashLog = await db.TrashLog.FindAsync(id);
            if (trashLog == null)
            {
                return NotFound();
            }

            db.TrashLog.Remove(trashLog);
            await db.SaveChangesAsync();

            return Ok(trashLog);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrashLogExists(Guid id)
        {
            return db.TrashLog.Count(e => e.Id == id) > 0;
        }
    }
}