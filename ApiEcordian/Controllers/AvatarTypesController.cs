﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class AvatarTypesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/AvatarTypes
        public IQueryable<AvatarType> GetAvatarTypes()
        {
            return db.AvatarTypes;
        }

        // GET: api/AvatarTypes/5
        [ResponseType(typeof(AvatarType))]
        public async Task<IHttpActionResult> GetAvatarType(Guid id)
        {
            AvatarType avatarType = await db.AvatarTypes.FindAsync(id);
            if (avatarType == null)
            {
                return NotFound();
            }

            return Ok(avatarType);
        }

        // PUT: api/AvatarTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAvatarType(Guid id, AvatarType avatarType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != avatarType.Id)
            {
                return BadRequest();
            }

            db.Entry(avatarType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AvatarTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AvatarTypes
        [ResponseType(typeof(AvatarType))]
        public async Task<IHttpActionResult> PostAvatarType(AvatarType avatarType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AvatarTypes.Add(avatarType);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AvatarTypeExists(avatarType.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = avatarType.Id }, avatarType);
        }

        // DELETE: api/AvatarTypes/5
        [ResponseType(typeof(AvatarType))]
        public async Task<IHttpActionResult> DeleteAvatarType(Guid id)
        {
            AvatarType avatarType = await db.AvatarTypes.FindAsync(id);
            if (avatarType == null)
            {
                return NotFound();
            }

            db.AvatarTypes.Remove(avatarType);
            await db.SaveChangesAsync();

            return Ok(avatarType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AvatarTypeExists(Guid id)
        {
            return db.AvatarTypes.Count(e => e.Id == id) > 0;
        }
    }
}