﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class QRConnectionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/QRConnections
        public IQueryable<QRConnection> GetQRConnections()
        {
            return db.QRConnections;
        }

        // GET: api/QRConnections/5
        [ResponseType(typeof(QRConnection))]
        public async Task<IHttpActionResult> GetQRConnection(Guid id)
        {
            QRConnection qRConnection = await db.QRConnections.FindAsync(id);
            if (qRConnection == null)
            {
                return NotFound();
            }

            return Ok(qRConnection);
        }

        // PUT: api/QRConnections/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutQRConnection(Guid id, QRConnection qRConnection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != qRConnection.Id)
            {
                return BadRequest();
            }

            db.Entry(qRConnection).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QRConnectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        public async Task<IHttpActionResult> Verify(Guid Userid, string QR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            QRConnection temp = (from p in db.QRConnections
                                 where p.QRCode == QR
                                 select p).FirstOrDefault();
            if(temp==null)
            {
                return NotFound();
            }
            else
            {
                temp.UserID = Userid;
                
            }

            db.Entry(temp).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QRConnectionExists(temp.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }



        // POST: api/QRConnections
        [ResponseType(typeof(QRConnection))]
        public async Task<IHttpActionResult> PostQRConnection(QRConnection qRConnection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.QRConnections.Add(qRConnection);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (QRConnectionExists(qRConnection.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = qRConnection.Id }, qRConnection);
        }

        // DELETE: api/QRConnections/5
        [ResponseType(typeof(QRConnection))]
        public async Task<IHttpActionResult> DeleteQRConnection(Guid id)
        {
            QRConnection qRConnection = await db.QRConnections.FindAsync(id);
            if (qRConnection == null)
            {
                return NotFound();
            }

            db.QRConnections.Remove(qRConnection);
            await db.SaveChangesAsync();

            return Ok(qRConnection);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QRConnectionExists(Guid id)
        {
            return db.QRConnections.Count(e => e.Id == id) > 0;
        }
    }
}