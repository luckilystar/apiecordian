﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class TrashesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Trashes
        public IQueryable<Trash> GetTrashes()
        {
            return db.Trashes;
        }

        // GET: api/Trashes/5
        [ResponseType(typeof(Trash))]
        public async Task<IHttpActionResult> GetTrash(Guid id)
        {
            Trash trash = await db.Trashes.FindAsync(id);
            if (trash == null)
            {
                return NotFound();
            }

            return Ok(trash);
        }

        // PUT: api/Trashes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTrash(Guid id, Trash trash)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trash.Id)
            {
                return BadRequest();
            }

            db.Entry(trash).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrashExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Trashes
        [ResponseType(typeof(Trash))]
        public async Task<IHttpActionResult> PostTrash(Trash trash)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Trashes.Add(trash);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TrashExists(trash.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = trash.Id }, trash);
        }

        // DELETE: api/Trashes/5
        [ResponseType(typeof(Trash))]
        public async Task<IHttpActionResult> DeleteTrash(Guid id)
        {
            Trash trash = await db.Trashes.FindAsync(id);
            if (trash == null)
            {
                return NotFound();
            }

            db.Trashes.Remove(trash);
            await db.SaveChangesAsync();

            return Ok(trash);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrashExists(Guid id)
        {
            return db.Trashes.Count(e => e.Id == id) > 0;
        }
    }
}