﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ApiEcordian.Models;

namespace ApiEcordian.Controllers
{
    public class AchievementTypesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/AchievementTypes
        public IQueryable<AchievementType> GetTypes()
        {
            return db.Types;
        }

        // GET: api/AchievementTypes/5
        [ResponseType(typeof(AchievementType))]
        public async Task<IHttpActionResult> GetAchievementType(Guid id)
        {
            AchievementType achievementType = await db.Types.FindAsync(id);
            if (achievementType == null)
            {
                return NotFound();
            }

            return Ok(achievementType);
        }

        // PUT: api/AchievementTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAchievementType(Guid id, AchievementType achievementType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != achievementType.Id)
            {
                return BadRequest();
            }

            db.Entry(achievementType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AchievementTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AchievementTypes
        [ResponseType(typeof(AchievementType))]
        public async Task<IHttpActionResult> PostAchievementType(AchievementType achievementType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Types.Add(achievementType);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AchievementTypeExists(achievementType.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = achievementType.Id }, achievementType);
        }

        // DELETE: api/AchievementTypes/5
        [ResponseType(typeof(AchievementType))]
        public async Task<IHttpActionResult> DeleteAchievementType(Guid id)
        {
            AchievementType achievementType = await db.Types.FindAsync(id);
            if (achievementType == null)
            {
                return NotFound();
            }

            db.Types.Remove(achievementType);
            await db.SaveChangesAsync();

            return Ok(achievementType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AchievementTypeExists(Guid id)
        {
            return db.Types.Count(e => e.Id == id) > 0;
        }
    }
}