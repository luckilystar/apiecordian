namespace ApiEcordian.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRewards : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AchievementRewards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AchievementId = c.Guid(nullable: false),
                        RewardId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Achievements", t => t.AchievementId, cascadeDelete: true)
                .ForeignKey("dbo.Rewards", t => t.RewardId, cascadeDelete: true)
                .Index(t => t.AchievementId)
                .Index(t => t.RewardId);
            
            CreateTable(
                "dbo.Achievements",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rewards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(maxLength: 250),
                        Type = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AchievementRewards", "RewardId", "dbo.Rewards");
            DropForeignKey("dbo.AchievementRewards", "AchievementId", "dbo.Achievements");
            DropIndex("dbo.AchievementRewards", new[] { "RewardId" });
            DropIndex("dbo.AchievementRewards", new[] { "AchievementId" });
            DropTable("dbo.Rewards");
            DropTable("dbo.Achievements");
            DropTable("dbo.AchievementRewards");
        }
    }
}
