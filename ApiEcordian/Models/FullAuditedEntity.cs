﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class FullAuditedEntity<TKey>
    {
        [Key]
        public TKey Id { get; set; }
        public Guid CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid ModifiedUserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid DeletedUserId { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool IsDeleted { get; set; }

        public FullAuditedEntity()
        {
            CreatedDate = DateTime.Now;
            IsDeleted = false;
        }
    }
}