﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Trash:FullAuditedEntity<Guid>
    {
        public bool IsEmpty { get; set; }
        public string Type { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Name { get; set; }

        public Trash()
        {
            Id = Guid.NewGuid();
            IsEmpty = true;
        }
    }
}