﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class AchievementRequirement:FullAuditedEntity<Guid>
    {
        public string Type { get; set; }
        public string Requirement { get; set; }
        public string Description { get; set; }


        public AchievementRequirement()
        {
            Id = Guid.NewGuid();
        }
    }
}