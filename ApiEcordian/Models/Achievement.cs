﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Achievement:FullAuditedEntity<Guid>
    {
        [MaxLength(250)]
        public string Name { get; set; }

        public ICollection<AchievementReward> AchievementRewards { get; set; }
        public ICollection<AchievementRequirement> AchievementRequirements { get; set; }


        public Achievement()
        {
            Id = Guid.NewGuid();
        }
    }
}