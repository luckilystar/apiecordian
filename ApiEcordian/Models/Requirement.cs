﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Requirement:FullAuditedEntity<Guid>
    {
    [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Type { get; set; }
        public Boolean StatusComplete { get; set; }

        public ICollection<AchievementRequirement> AchievementRequirements { get; set; }


        public Requirement()
        {
            Id = Guid.NewGuid();
        }
    }
}