﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class TrashLog : FullAuditedEntity<Guid>
    {
        
        public Trash Trash { get; set; }

        public Person Person { get; set; }

        public Guid TrashID { get; set; }
        public Guid PersonID { get; set; }

        public DateTime Time { get; set; }

        public TrashLog()
        {
            Id = Guid.NewGuid();
        }

    }
}