<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Reward
    {
        [Key]
        public Guid Id { get; set; }

        [StringLength(250)]
        public string Name { get; set; }
        public Reward()
        {
            Id = new Guid();
        }
    }
=======
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Reward:FullAuditedEntity<Guid>
    {

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Type { get; set; }

        public ICollection<AchievementReward> AchievementRewards { get; set; }


        public Reward()
        {
            Id = Guid.NewGuid();
        }
    }
>>>>>>> 7fca899498c596f656efac8ccdcf2d32c6303014
}