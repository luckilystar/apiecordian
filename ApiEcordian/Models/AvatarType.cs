﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class AvatarType:FullAuditedEntity<Guid>
    {
        public string Name { get; set; }


        public AvatarType()
        {
            Id = Guid.NewGuid();
        }
    }
}