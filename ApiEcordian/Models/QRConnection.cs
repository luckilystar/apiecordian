﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class QRConnection : FullAuditedEntity<Guid>
    {
        public Guid UserID { get; set; }
        public Person Person { get; set; }
        public Guid TrashID { get; set; }
        public Trash Trash { get; set; }
        public string QRCode { get; set; }
        public int Total { get; set; }
        public QRConnection()
        {
            Id = Guid.NewGuid();
        }
    }
}