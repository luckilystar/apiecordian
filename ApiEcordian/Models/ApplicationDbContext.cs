﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ApiEcordian.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<Reward> Rewards { get; set; }

        public DbSet<AchievementReward> AchievementRewards { get; set; }

        public DbSet<AchievementRequirement> AchievementRequirements { get; set; }
        public DbSet<Avatar> Avatars { get; set; }
        public DbSet<AvatarType> AvatarTypes { get; set; }
        public DbSet<Background> Backgrounds { get; set; }
        public DbSet<Requirement> Requirements { get; set; }
        public DbSet<Trash> Trashes { get; set; }
        public DbSet<AchievementType> Types { get; set; }
        public DbSet<Person> Persons { get; set; }

        public DbSet<TrashLog> TrashLog { get; set; }

        public DbSet<QRConnection> QRConnections { get; set; }

        //public System.Data.Entity.DbSet<ApiEcordian.Models.QRConnection> QRConnections { get; set; }
        //public DbSet<User> Users { get; set; }
    }
}