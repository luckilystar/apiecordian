﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Avatar:FullAuditedEntity<Guid>
    {
       public string Name { get; set; }
        public float Price { get; set; }
        public string ImageUrl { get; set; }

        public AvatarType AvatarType { get; set; }
        public Guid AvatarTypeId { get; set; }


        public Avatar()
        {
            Id = Guid.NewGuid();
        }
    }
}