﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class AchievementReward:FullAuditedEntity<Guid>
    {
        public Guid AchievementId { get; set; }
        public Guid RewardId { get; set; }
        public Achievement Achievement { get; set; }
        public Reward Reward { get; set; }


        public AchievementReward()
        {
            Id = Guid.NewGuid();
        }
    }
}