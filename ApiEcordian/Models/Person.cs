﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Person //: FullAuditedEntity<Guid>
    {
        [Key]
        public Guid Id { get; set; }
        //public Guid BackgroundId { get; set; }
        //public Background Background { get; set; }

        //public Guid HeadId { get; set; }
       
        //public Guid IdShirt { get; set; }
        //public Guid IdSkirt { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        public int Money { get; set; }

        //public Guid UserId { get; set; }
        //public User User { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }


        public Person()
        {
            Id = Guid.NewGuid();
        }
    }
}