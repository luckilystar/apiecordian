﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class AchievementType:FullAuditedEntity<Guid>
    {
        [MaxLength(200)]
        public string Name { get; set; }
        public Guid ImageURL { get; set; } //g tau type buat image :'D 

        public ICollection<AvatarType> AvatarTypes { get; set; }


        public AchievementType()
        {
            Id = Guid.NewGuid();
        }
        public AchievementType(string name)
        {
            Name = name;
        }
    }
}