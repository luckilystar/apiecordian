﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiEcordian.Models
{
    public class Background:FullAuditedEntity<Guid>
    {
        public string ImageUrl { get; set; } 
        public Guid IdAvatar { get; set; }


        public Background()
        {
            Id = Guid.NewGuid();
        }
    }
}